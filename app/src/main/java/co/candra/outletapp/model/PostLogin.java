package co.candra.outletapp.model;

import com.google.gson.annotations.SerializedName;

public class PostLogin {
    @SerializedName("email")
    private String email;
    @SerializedName("password")
    private String password;

    public PostLogin(String email, String password) {
        this.email = email;
        this.password = password;
    }
}
