package co.candra.outletapp.model

data class User(
    var email: String? = null,
    var password: String? = null
)