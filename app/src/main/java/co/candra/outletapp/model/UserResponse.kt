package co.candra.outletapp.model

data class UserResponse(
    var code: Int? = null,
    var message: String? = null,
    var status: String? = null
)