package co.candra.outletapp.model

import com.google.gson.annotations.SerializedName

data class Outlet(
    @SerializedName("code")
    var code: String,

    @SerializedName("name")
    var name: String,

    @SerializedName("addess")
    var address: String,

    @SerializedName("lat")
    var latitude: Double,

    @SerializedName("lng")
    var longitude: Double,

    @SerializedName("owner")
    var owner: String
)
