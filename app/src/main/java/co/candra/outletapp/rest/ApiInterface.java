package co.candra.outletapp.rest;



import java.util.List;

import co.candra.outletapp.model.BaseResponse;
import co.candra.outletapp.model.Outlet;
import co.candra.outletapp.model.PostLogin;
import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.POST;
import retrofit2.http.GET;


public interface ApiInterface {

    @POST("api/login")
    Call<BaseResponse> postLogin(@Body PostLogin postLogin);

    @GET("api/outlet")
    Call< List<Outlet>> getOutlet();

}