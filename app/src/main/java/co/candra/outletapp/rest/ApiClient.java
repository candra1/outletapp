package co.candra.outletapp.rest;

import androidx.annotation.NonNull;

import java.io.IOException;

import okhttp3.Interceptor;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class ApiClient {
    public static final String BASE_URL = "http://18.139.222.3:9801";
    public static String API_KEY = "869e3209-10f8-42e3-a7c9-7cf36d943f53";
    private static Retrofit retrofit = null;
    public static Retrofit getClient(){
        if(retrofit==null){
            OkHttpClient client= new OkHttpClient().newBuilder().addInterceptor(new Interceptor(){
                @Override
                public Response intercept(@NonNull Chain chain ) throws IOException {
                    Request request = chain.request()
                            .newBuilder()
                            .addHeader ("Authorization", API_KEY)
                            .build();
                    return chain.proceed(request);
                }
            }).build();
            retrofit=new Retrofit.Builder()
                    .client(client)
                    .baseUrl(BASE_URL)
                    .addConverterFactory(GsonConverterFactory.create())
                    .build();
        }
        return retrofit;
    }
}