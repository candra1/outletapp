package co.candra.outletapp

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import android.widget.Toast
import co.candra.outletapp.model.User
import kotlinx.android.synthetic.main.activity_login.*

class LoginActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_login)
        onClickGroup()

    }
    fun onClickGroup(){
        loginButton.setOnClickListener {
            if(usernameEditText.text.isEmpty() || passwordEditText.text.isEmpty()){
                Toast.makeText(this,"Username/password is empty!",Toast.LENGTH_SHORT).show()
            }
            else {
                var user = User(usernameEditText.text.toString(), passwordEditText.text.toString())
                if (getLogin(user)) {
                    val intent = Intent(this, HomeActivity::class.java)
                    startActivity(intent)
                } else {
                    val intent = Intent(this, DialogActivity::class.java)
                    intent.apply {
                        putExtra("pesan", "Login gagal! Username/password salah!")
                    }
                    startActivity(intent)
                }
            }
        }
    }


    fun getLogin(user: User): Boolean{
        Log.d("getLogin ",user.email)
        return true
    }
}
