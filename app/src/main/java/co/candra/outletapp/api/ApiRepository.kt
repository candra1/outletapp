package co.candra.outletapp.api

import co.candra.outletapp.model.BaseResponse
import co.candra.outletapp.model.Outlet
import okhttp3.OkHttpClient
import okhttp3.Request
import retrofit2.Call
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import retrofit2.http.GET

//TODO UBAH DENGAN YANG DIBUAT WILLIAM KEVIN
interface ApiRepository {
    @GET("api/outlet")
    fun getOutlets() : Call<BaseResponse<List<Outlet>>>

    companion object {
        val BASE_URL = "http://18.139.222.3:9801"
        val API_KEY = "xx"

        fun create(): ApiRepository {
            val client = OkHttpClient().newBuilder()
                .addInterceptor {
                    val request: Request = it.request()
                        .newBuilder().addHeader("Authorization", API_KEY).build()
                    return@addInterceptor it.proceed(request)
                }
                .build()

            val retrofit = Retrofit.Builder()
                .client(client)
                .addConverterFactory(
                    GsonConverterFactory.create()
                )
                .baseUrl(BASE_URL)
                .build()

            return retrofit.create(ApiRepository::class.java)
        }
    }
}