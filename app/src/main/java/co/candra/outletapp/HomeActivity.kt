package co.candra.outletapp

import android.annotation.TargetApi
import android.os.Build
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import android.view.View
import android.widget.Toast
import co.candra.outletapp.api.ApiRepository
import co.candra.outletapp.model.BaseResponse
import co.candra.outletapp.model.Outlet
import com.google.android.gms.maps.CameraUpdateFactory
import com.google.android.gms.maps.GoogleMap
import com.google.android.gms.maps.OnMapReadyCallback
import com.google.android.gms.maps.SupportMapFragment
import com.google.android.gms.maps.model.LatLng
import com.google.android.gms.maps.model.Marker
import com.google.android.gms.maps.model.MarkerOptions
import kotlinx.android.synthetic.main.activity_home.*
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import java.util.*

class HomeActivity : AppCompatActivity(), OnMapReadyCallback, GoogleMap.OnMarkerClickListener {
    private lateinit var googleMap: GoogleMap
//    private var listOutlet = arrayListOf<Outlet>()

    var listOutlet: List<Outlet>? = null
    private val apiRepository by lazy {
        ApiRepository.create()//TODO UBAH apiRepository DENGAN YANG DIBUAT WILLIAM KEVIN
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_home)
        initData()
        clickGroup()
    }
    fun clickGroup(){
        closeInformationButton.setOnClickListener {
            informationLinearLayout.visibility = View.GONE
        }
    }

//    fun getListOutlet() : List<Outlet>?{
//        return listOf(
//            Outlet(0, "Outlet 1", "Cibubur 1", -6.363857, 106.958995, "Imam"),
//            Outlet(1, "Outlet 2", "Cibubur 2", -6.367925, 106.893841, "Arif"),
//            Outlet(2, "Outlet 3", "Cibubur 3", -6.375158, 106.912142, "Budi"),
//            Outlet(3, "Outlet 4", "Cibubur 4", -6.381312, 106.924523, "Ahmad"),
//            Outlet(4, "Outlet 5", "Cibubur 5", -6.395935, 106.943833, "Rocky")
//            )
//    }

    fun initData(){
        askPermissions()
        startTimerTaskGetListOutlet()
        val mapFragment = supportFragmentManager.findFragmentById(R.id.mapFragment) as SupportMapFragment
        mapFragment.getMapAsync(this)
        getListOutlet()
    }

    fun markerMapp(){
        if (googleMap != null) {
            val zoomLevel = 12.0f
            googleMap.let { map ->
                listOutlet!!.forEach {
                    val latLng = LatLng(it.latitude, it.longitude)
                    val markerOptions = MarkerOptions().position(latLng).title(it.name)
                    map.addMarker(markerOptions)
                }
                val defaultLatLng = LatLng(listOutlet!![0].latitude, listOutlet!![0].longitude)
                Log.d("tag", "lat = ${defaultLatLng.latitude} long = ${defaultLatLng.longitude}")
                map.moveCamera(CameraUpdateFactory.newLatLngZoom(defaultLatLng, zoomLevel))
                map.setOnMarkerClickListener(this)
            }
        }

    }
    override fun onMapReady(p0: GoogleMap?) {
        googleMap = p0!!

        getListOutlet()
    }

    fun getListOutlet() {//Tidak saya return karna yg dikerjakan duluan yg return baru seletah itu api call
        Log.d("getListOutlet() ","1")
        //TODO UBAH apiRepository DENGAN YANG DIBUAT WILLIAM KEVIN
        apiRepository.getOutlets().enqueue(object : Callback<BaseResponse<List<Outlet>>> {
            override fun onFailure(call: Call<BaseResponse<List<Outlet>>>, t: Throwable) {
                Toast.makeText(
                    this@HomeActivity,
                    getString(R.string.msg_getoutlet_error),
                    Toast.LENGTH_SHORT
                ).show()
                Log.d("getOutlet", t.message)
            }

            override fun onResponse(
                call: Call<BaseResponse<List<Outlet>>>,
                response: Response<BaseResponse<List<Outlet>>>
            ) {
                if (response.isSuccessful && response.body()!!.data.isNotEmpty()) {
                    listOutlet = response.body()!!.data

                    Log.d("getOutlet", "listOutlet -> $listOutlet")

                    listOutlet?.forEach {
                        Log.d("getOutlet", "name -> ${it.name}")
                    }
                    markerMapp()

                } else {
                    Toast.makeText(
                        this@HomeActivity,
                        getString(R.string.msg_getoutlet_empty),
                        Toast.LENGTH_SHORT
                    ).show()
                }
            }

        })
    }

    override fun onMarkerClick(p0: Marker?): Boolean {
        Log.d("tag", "clicked ${p0?.title}")
        p0?.showInfoWindow()
        listOutlet!!.forEach {
            if (p0?.title == it.name){
                showOutletInfo(it)
                return true
            }
        }
        return false
    }

    private fun showOutletInfo(outlet: Outlet){
        Log.d("ShowOutletInfo ",outlet.name)
        nameOutletTextView.text = outlet.name
        addressOutletTextView.text = outlet.address
        ownerOutletTextView.text = outlet.owner
        codeOutletTextView.text = outlet.code.toString()
        informationLinearLayout.visibility = View.VISIBLE


    }
    @TargetApi(23)
    fun askPermissions() {
        val permissions = arrayOf(
            "android.permission.ACCESS_COARSE_LOCATION",
            "android.permission.ACCESS_FINE_LOCATION",
            "android.permission.INTERNET"
        )
        requestPermissions(permissions, 200);
    }
    fun startTimerTaskGetListOutlet() {
        var timer = Timer();
        var task =  object : TimerTask() {
            override fun run() {
                getListOutlet()
            }
        };
        timer.schedule(task, 0, 1000 * 2);
    }
}
